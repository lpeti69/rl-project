import torch
import torch.nn.functional as F
from torch import nn
from torch.distributions import Categorical
from torch.autograd import Variable
import numpy as np
from collections import OrderedDict

class A2CNet(nn.Module):
    def __init__(self, action_space = 3, num_of_training_epochs = 10000, image_size = 200, device = 'cuda'):
        super(A2CNet, self).__init__()
        self.action_space = action_space
        self.train_device = device

        self.body = self.build_body()
        self.actor = self.build_actor_head()
        self.critic = self.build_critic_head()

        self.init_weights()
        self.middle_linear = nn.Linear(32*11*11, 256)
        self.optimizer = torch.optim.RMSprop(self.parameters(), lr=2e-4)

        self.decayer = lambda f: 1 - f/num_of_training_epochs
        self.scheduler = torch.optim.lr_scheduler.LambdaLR(
            self.optimizer, 
            lr_lambda=self.decayer
        )


    def build_body(self):  

        return nn.Sequential(
                nn.Conv2d(5, 16, 5, 2),
                nn.ReLU(),
                nn.Conv2d(16, 32, 4, 1),
                nn.ReLU(),
                nn.Conv2d(32, 64, 3, 1),
                SpatialSoftmax(64, 43, 43, self.train_device)
                #nn.Conv2d(2, 16, 5, 2),
                #nn.ReLU(),
                #nn.Conv2d(16, 32, 4, 2),
                #nn.ReLU(),
                #nn.Conv2d(32, 64, 3, 1),
                #SpatialSoftmax(64, 21, 21, self.train_device)
            )

    def build_actor_head(self):
        return nn.Sequential(
            nn.Linear(
                128,
                self.action_space
            )
            #nn.Linear(
            #    128,
            #    self.action_space
            #)
        )

    def build_critic_head(self):
        return nn.Sequential(
            nn.Linear(
                128,
                1
            )
            #nn.Linear(
            #    128,
            #    1
            #)
        )

    def init_weights(self):
        for m in self.modules():
            if type(m) in [nn.Linear, nn.Conv2d]:
                nn.init.normal_(m.weight, 0, 9e-4)
                nn.init.zeros_(m.bias)

    def forward(self, x):
        x = self.body(x)
        #x = x.view(-1, 32*11*11)
#
        #x = self.middle_linear(x)
        
        x_mean = self.actor(x)
        value = self.critic(x)
        
        x_probs = F.softmax(x_mean, dim=-1)
        dist = Categorical(x_probs)

        return dist, value


class SpatialSoftmax(nn.Module):
    def __init__(self, channel, height, width, train_device):
        super(SpatialSoftmax, self).__init__()
        self.height = height
        self.width = width
        self.channel = channel
        self.train_device = train_device

        pos_x, pos_y = np.meshgrid(
            np.linspace(-1., 1., self.height),
            np.linspace(-1., 1., self.width)
        )

        pos_x = torch.from_numpy(pos_x.reshape(self.height*self.width)).float()
        pos_y = torch.from_numpy(pos_y.reshape(self.height*self.width)).float()
        self.pos_x = pos_x
        self.pos_y = pos_y

    def forward(self, feature):
        feature = feature.view(-1, self.height*self.width)
        softmax_attention = F.softmax(feature, dim=-1)

        expected_x = torch.sum(Variable(self.pos_x).to(self.train_device)*softmax_attention.to(self.train_device), dim=1, keepdim=True)
        expected_y = torch.sum(Variable(self.pos_y).to(self.train_device)*softmax_attention.to(self.train_device), dim=1, keepdim=True)
        expected_xy = torch.cat([expected_x, expected_y], 1)
        
        feature_keypoints = expected_xy.view(-1, self.channel*2)
        return feature_keypoints