import matplotlib.pyplot as plt
from random import randint
import pickle
import gym
import numpy as np
import torch
import argparse
import wimblepong
from .agent_ppo_maybe import Agent
from .utils import plot_rewards, generalized_advantage_estimate, normalize, discount_rewards
import pandas as pd
import seaborn as sns

class Train():
    def __init__(
        self,
        headless = False,
        housekeeping = False,
        fps = 30,
        scale = 1,
        train_device = 'cpu',
        is_training = True,
        load_params = True
    ):
        # Make the environment
        self.env = gym.make("WimblepongVisualSimpleAI-v0")
        self.env.unwrapped.scale = scale
        self.env.unwrapped.fps = fps
        self.housekeeping = housekeeping
        self.headless = headless
        self.train_device = train_device
        self.update_frequency = 96
        self.rollout_length = 10

        # Number of episodes/games to play
        self.episodes = 100000

        # Define the player
        self.player_id = 1
        # Set up the player here. We used the SimpleAI that does not take actions for now
        self.player = Agent(
            train_device=train_device, 
            is_training=is_training,
            load_params=load_params
        )
        self.env.set_names(self.player.get_name(), "SimpleAI")

        # Housekeeping
        self.win1 = 0
        self.states, self.reward_history, self.timestemp_history, self.average_reward_history = \
            [], [], [], []
    
    def run(self):
        self.T = 1000
        self.step_counter = 0
        for episode_number in range(0, self.episodes):
            self.states, self.values, self.dones, self.rewards, self.actions, self.action_log_probs = \
                [], [], [], [], [], []
            done = False
            self.timestemps = 0
            state = self.env.reset()
            state = self.player.preprocess(state).to(self.train_device)
            while not done:
                action, action_log_prob, value = self.player.get_action(state)
                prev_state = state
                state, reward, done, _info = self.env.step(action)
                state = self.player.preprocess(state).to(self.train_device)

                self.states.append(prev_state)
                self.actions.append(action)
                self.values.append(value)
                self.action_log_probs.append(action_log_prob)
                self.dones.append(torch.tensor(int(done)))
                self.rewards.append(torch.tensor(reward))

                self.timestemps += 1
                self.step_counter += 1
                if self.housekeeping:
                    #self.states.append(state)
                    pass
                # Count the wins
                if reward == 10:
                    self.win1 += 1
                if not self.headless or episode_number > self.episodes + 25:
                    self.env.render()
                if done:
                    states, actions, action_log_probs, values, dones, rewards = \
                        torch.stack(self.states).to(self.train_device), \
                        torch.stack(self.actions).to(self.train_device), \
                        torch.stack(self.action_log_probs).to(self.train_device), \
                        torch.stack(self.values).squeeze().to(self.train_device), \
                        torch.stack(self.dones).to(self.train_device), \
                        torch.stack(self.rewards).to(self.train_device)
                    
                    _, next_value = self.player.a2c.forward(state)
                    next_values = torch.cat([values[1:], next_value[0]]).to(self.train_device)
                    next_values =  (1 - dones) * next_values

                    q_values = rewards + self.player.gamma * next_values
                    td_errors = q_values - values

                    advantages = generalized_advantage_estimate(td_errors, gamma=self.player.gamma, _lambda=0)\
                        .to(self.train_device)

                    value_target = advantages + values
                    advantages = normalize(advantages)

                    #print(action_log_probs.shape)
                    for i in range(self.timestemps):
                        self.player.store(
                            states = states[i],
                            actions = actions[i],
                            action_log_probs = action_log_probs[i],
                            advantages = advantages[i],
                            value_targets = value_target[i]
                        )
                    self.player.batch_update()
                    plt.close()  # Hides game window
                    if self.housekeeping:
                        #plt.plot(states)
                        #plt.legend(["Player", "Opponent", "Ball X", "Ball Y", "Ball vx", "Ball vy"])
                        #plt.show()
                        #states.clear()
                        pass
                    self.reward_history.append(reward)
                    self.timestemp_history.append(self.timestemps)
                    if episode_number > 100:
                        avg = np.mean(self.reward_history[-100:])
                    else:
                        avg = np.mean(self.reward_history)
                    self.average_reward_history.append(avg)
                    print("episode {} over. Avg Reward: {:.3f}, Reward: {}, Broken WR: {:.3f}, Length: {}"
                            .format(episode_number, avg, int(reward), self.win1/(episode_number+1), self.timestemps))
                    if episode_number % 5 == 4:
                        self.env.switch_sides()
                    #if episode_number % 10 == 0 and episode_number > 1:
                    #    self.player.update()
                #if self.step_counter >= self.player.T and episode_number > self.rollout_length:
                #    self.player.batch_update()
                #    self.step_counter = 0


            if self.housekeeping:
                if (episode_number % 1000 == 0 and episode_number >= 1000) or episode_number == self.episodes - 1:
                    self.show_result(episode_number)

    def save(self):
        _data = pd.DataFrame({"episode": np.arange(len(self.reward_history)),
                            "algorithm": ["PG"]*len(self.reward_history),
                            "reward": self.reward_history})
        torch.save(self.player.a2c.state_dict(), "model_pogchamp_ppo_gae_paper_netw.mdl")

    def show_result(self, episode_number):
        sns.set()
        sns.set_style('darkgrid')
        plt.legend(["Reward", "100-episode average"])
        plt.title("Reward history")

        sns.tsplot(data=self.reward_history, value='reward', condition='reward', ci='sd', color='g')
        sns.tsplot(data=self.average_reward_history, value='reward', condition='Avg reward', ci='sd', color='b')
        plt.savefig('training_ppo_gae_paper_netw.png')
        
        if episode_number == self.episodes - 1:
            plt.show()
            print("Training finished.")
        print('SAVE')
        self.save()