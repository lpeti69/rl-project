import matplotlib.pyplot as plt
from random import randint
import pickle
import gym
import numpy as np
import torch
import argparse
import wimblepong
from .agent_ppo import Agent
from .utils import plot_rewards, generalized_advantage_estimate, normalize, discount_rewards
import pandas as pd
import seaborn as sns

class Train():
    def __init__(
        self,
        headless = False,
        housekeeping = False,
        fps = 30,
        scale = 1,
        train_device = 'cpu',
        is_training = True,
        load_params = True
    ):

        self.env = gym.make("WimblepongVisualSimpleAI-v0")
        self.env.unwrapped.scale = scale
        self.env.unwrapped.fps = fps
        self.housekeeping = housekeeping
        self.headless = headless
        self.train_device = train_device
        self.file_name = 'pongchamp_ppo_gae_full'
        self.num_of_game_episodes = 100000
        self.player_id = 1
        self.player = Agent(
            train_device=train_device, 
            is_training=is_training,
            load_params=load_params,
            file_name=self.file_name
        )
        self.env.set_names(self.player.get_name(), "SimpleAI")
        self.my_num_of_win = 0
        self.reward_history, self.average_reward_history, self.win_rate_history, self.avg_win_rate_history = \
            [], [], [], []
    
    def run(self):

        for episode_number in range(self.num_of_game_episodes):
            self.states, self.values, self.dones, self.rewards, self.actions, self.action_log_probs = \
                [], [], [], [], [], []
            done = False
            self.timestemps = 0
            state = self.env.reset()
            state = self.player.preprocess(state).to(self.train_device)
            while not done:
                action, action_log_prob, value = self.player.get_action(state)
                prev_state = state
                state, reward, done, _info = self.env.step(action)
                state = self.player.preprocess(state).to(self.train_device)

                self.states.append(prev_state)
                self.actions.append(action)
                self.values.append(value)
                self.action_log_probs.append(action_log_prob)
                self.dones.append(torch.tensor(int(done)))
                self.rewards.append(torch.tensor(reward))

                self.timestemps += 1

                if reward == 10:
                    self.my_num_of_win += 1
                if not self.headless or episode_number > self.num_of_game_episodes - 25:
                    self.env.render()
                if done:
                    self.store_episode(state)
                    self.eval_episode_results(episode_number, reward)

                    if episode_number % 5 == 4:
                        self.env.switch_sides()

                if self.player.memory.size >= self.player.batch_size:
                    self.player.batch_update()

            if self.housekeeping:
                if (episode_number % 1000 == 0 and episode_number >= 1000) or episode_number == self.num_of_game_episodes - 1:
                    self.show_result(episode_number)
                    self.save_agent_params()

    def eval_episode_results(self, episode_number, reward):

        win_rate = self.my_num_of_win/(episode_number + 1)

        self.win_rate_history.append(win_rate)
        self.reward_history.append(reward / 10)

        avg_reward = np.mean(self.reward_history[-100:])
        avg_winrate = np.mean(self.win_rate_history[-100:])

        self.avg_win_rate_history.append(avg_winrate)
        self.average_reward_history.append(avg_reward)

        result = '+' if int(reward) == 10 else '-'
        print("episode {} over. Avg Reward: {:.3f}, Result: {}, Broken WR: {:.3f}, Length: {}"
                .format(episode_number, avg_reward, result, self.my_num_of_win/(episode_number + 1), self.timestemps))

    def store_episode(self, state):
        states, actions, action_log_probs, values, dones, rewards = [
            torch.stack(item).to(self.train_device)
                for item in [
                    self.states,
                    self.actions,
                    self.action_log_probs,
                    self.values,
                    self.dones,
                    self.rewards
                ]
        ]
        values = values.squeeze()

        _, next_value = self.player.a2c.forward(state)
        next_values = torch.cat([values[1:], next_value[0]]).to(self.train_device)
        next_values =  (1 - dones) * next_values

        q_values = rewards + self.player.gamma * next_values
        td_errors = q_values - values

        advantages = generalized_advantage_estimate(td_errors, gamma=self.player.gamma, _lambda=0.95)\
            .to(self.train_device)

        value_target = advantages + values
        advantages = normalize(advantages)

        for i in range(self.timestemps):
            self.player.store(
                states = states[i],
                actions = actions[i],
                action_log_probs = action_log_probs[i],
                advantages = advantages[i],
                value_targets = value_target[i],
                values = values[i],
                dones = dones[i]
            )

    def save_agent_params(self):
        _data = pd.DataFrame({"episode": np.arange(len(self.reward_history)),
                            "algorithm": ["PG"]*len(self.reward_history),
                            "reward": self.reward_history})
        torch.save(self.player.a2c.state_dict(), self.file_name + '.mdl')

    def show_result(self, episode_number):
        sns.set()
        sns.set_style('darkgrid')
        plt.legend(["Reward", "100-episode average"])
        plt.title("History")

        sns.tsplot(data=self.avg_win_rate_history, condition='Win rate', ci='sd', color='g')
        sns.tsplot(data=self.average_reward_history, condition='Avg reward', ci='sd', color='b')
        plt.savefig(self.file_name + '_results.png')
        
        if episode_number == self.episodes - 1:
            plt.show()
            print("Training finished.")