import matplotlib.pyplot as plt
from random import randint
import pickle
import gym
import numpy as np
import torch
import argparse
import wimblepong
from .agent_ppo import Agent
from .utils import plot_rewards
import pandas as pd
import seaborn as sns

class Train():
    def __init__(
        self,
        headless = False,
        housekeeping = False,
        fps = 30,
        scale = 1,
        train_device = 'cpu',
        is_training = True,
        load_params = True
    ):
        # Make the environment
        self.env = gym.make("WimblepongVisualSimpleAI-v0")
        self.env.unwrapped.scale = scale
        self.env.unwrapped.fps = fps
        self.housekeeping = housekeeping
        self.headless = headless

        # Number of episodes/games to play
        self.episodes = 100000

        # Define the player
        self.player_id = 1
        # Set up the player here. We used the SimpleAI that does not take actions for now
        self.player = Agent(
            train_device=train_device, 
            is_training=is_training,
            load_params=load_params
        )
        self.env.set_names(self.player.get_name(), "SimpleAI")

        # Housekeeping
        self.win1 = 0
        self.states, self.reward_history, self.timestemp_history, self.average_reward_history = \
            [], [], [], []
    
    def run(self):

        for episode_number in range(0, self.episodes):
            done = False
            self.timestemps = 0
            state = self.env.reset()
            while not done:
                transf_state, action, action_log_probs, entropy, value = self.player.get_action(state)
                prev_state = transf_state
                state, reward, done, _info = self.env.step(action)
                self.player.store(
                    states = prev_state,
                    actions = action,
                    action_log_probs = action_log_probs,
                    entropys = entropy,
                    next_states = transf_state, 
                    rewards = torch.Tensor([reward]),
                    dones = torch.Tensor([done]),
                    values = value
                )
                self.timestemps += 1
                if self.housekeeping:
                    #self.states.append(state)
                    pass
                # Count the wins
                if reward == 10:
                    self.win1 += 1
                if not self.headless or episode_number > self.episodes + 25:
                    self.env.render()
                if done:
                    ##self.player.update()
                    plt.close()  # Hides game window
                    if self.housekeeping:
                        #plt.plot(states)
                        #plt.legend(["Player", "Opponent", "Ball X", "Ball Y", "Ball vx", "Ball vy"])
                        #plt.show()
                        #states.clear()
                        pass
                    self.reward_history.append(reward)
                    self.timestemp_history.append(self.timestemps)
                    if episode_number > 100:
                        avg = np.mean(self.reward_history[-100:])
                    else:
                        avg = np.mean(self.reward_history)
                    self.average_reward_history.append(avg)
                    print("episode {} over. Avg Reward: {}, Reward: {}, Broken WR: {:.3f}, Length: {}"
                            .format(episode_number, avg, int(reward), self.win1/(episode_number+1), self.timestemps))
                    if episode_number % 5 == 4:
                        self.env.switch_sides()
                    if episode_number % 10 == 0 and episode_number > 1:
                        self.player.update()

            if self.housekeeping:
                if (episode_number % 1000 == 0 and episode_number >= 1000) or episode_number == self.episodes - 1:
                    self.show_result(episode_number)

    def save(self):
        _data = pd.DataFrame({"episode": np.arange(len(self.reward_history)),
                            "algorithm": ["PG"]*len(self.reward_history),
                            "reward": self.reward_history})
        torch.save(self.player.a2c.state_dict(), "model_pogchamp_ppo.mdl")

    def show_result(self, episode_number):
        sns.set()
        sns.set_style('darkgrid')
        plt.legend(["Reward", "100-episode average"])
        plt.title("Reward history")

        sns.tsplot(data=self.reward_history, value='reward', condition='reward', ci='sd', color='g')
        sns.tsplot(data=self.average_reward_history, value='reward', condition='Avg reward', ci='sd', color='b')
        plt.savefig('training_ppo.png')
        
        if episode_number == self.episodes - 1:
            plt.show()
            print("Training finished.")
        if episode_number % 2500 == 0 or episode_number == self.episodes - 1:
            self.save()