import torch
import numpy as np
from torch.distributions import Categorical
from .models_ppo import A2CNet
from .utils import ReplayMemory, normalize, discount_rewards, generalized_advantage_estimate
from torch.functional import F
import random
from torchvision.transforms import functional
from copy import deepcopy
from torch.autograd import Variable
from torch.nn import SmoothL1Loss
from torch.nn.utils import clip_grad_norm_
import seaborn as sns
from PIL import Image
import matplotlib.pyplot as plt

class Agent(object):
    def __init__(self, train_device = 'cpu', load_params = 'True', is_training = True, file_name = 'model'):
        self.train_device = train_device
        self.K_epochs = 10
        self.transformed_image_size = 100
        self.num_of_previous_frames = 5
        self.num_of_training_epochs = self.K_epochs * 25000
        self.eps_clip = 0.2
        self.gamma = 0.99
        self.batch_size = 2048
        self.trained_epochs = 0
        self.num_of_mini_batches = 32
        self.max_grad_norm = 0.5
        self.entropy_coeff = 0.01
        self.critic_coeff = 0.5
        self.is_training = is_training
        self.file_name = file_name
        self.a2c = A2CNet(device=train_device, 
                    num_of_training_epochs=self.num_of_training_epochs).to(self.train_device)
        self.losses = ReplayMemory(250, 'cl', 'al', 'el', 'kl')
        self.memory = ReplayMemory(self.batch_size, 'states', 'actions', 'action_log_probs', 'advantages', 'value_targets', 'values', 'dones')
        self.avg_critic_loss, self.avg_actor_loss, self.avg_entropy_loss, self.kl_divergences = [], [], [], []
        self.reset()

        if load_params:
            self.load_model_params()
        self.a2c.train(is_training)

    def load_model_params(self):
        state_dict = torch.load(
            self.file_name + '.mdl', 
            map_location=lambda storage, loc: storage
        )
        self.a2c.load_state_dict(state_dict)

    def get_action(self, observation):
        action_dist, value = self.a2c.forward(observation)

        action = action_dist.sample() if self.is_training else torch.argmax(action_dist.probs)
        action_log_probs = action_dist.log_prob(action)

        return action, action_log_probs, value

    def store(self, **kwargs):
        return self.memory.push(**kwargs)

    def reset(self):
        self.prev_obs = torch.zeros((
            self.num_of_previous_frames, 
            self.transformed_image_size, 
            self.transformed_image_size
        ))

    def get_name(self):
        return 'PongChamp'

    def batch_update(self):
        if self.is_training:

            states, actions, action_log_probs, advantages, value_targets, values, dones = \
                self.get_batch_history()

            for _ in range(self.K_epochs):

                self.trained_epochs += 1

                batch_indices = np.arange(self.memory.size)
                np.random.shuffle(batch_indices)
                batch_indices = np.array_split(batch_indices, self.num_of_mini_batches)

                for mini_batch_indeces in batch_indices:

                    mini_batch_experience = \
                        self.collect_mini_batch_from_experience(
                            mini_batch_indeces,
                            states,
                            actions,
                            action_log_probs,
                            advantages,
                            value_targets,
                            values,
                            dones
                        )

                    loss_critic, loss_actor, loss_entropy, kl_divergence = self.calculate_losses(*mini_batch_experience)

                    self.losses.push(
                        cl = loss_critic,
                        al = loss_actor,
                        el = loss_entropy,
                        kl = kl_divergence  
                    )

                    self.update_params(loss_critic, loss_actor, loss_entropy)
            
                avg_critic_loss, avg_actor_loss, avg_entropy_loss, kl_divergence = \
                    [
                        torch.FloatTensor(loss).mean().data
                            for loss in self.losses.get()
                    ]

                self.avg_critic_loss.append(avg_critic_loss)
                self.avg_actor_loss.append(abs(avg_actor_loss))
                self.avg_entropy_loss.append(avg_entropy_loss)
                self.kl_divergences.append(kl_divergence)

                if self.trained_epochs % 2 == 0:
                    print(
                        'Epoch: {}, Critic loss: {:.3f}, Actor loss: {:.5f}, Entropy loss: {:.3f}, Sym-KL-Divergence: {:.4f}'
                        .format(
                            self.trained_epochs,
                            avg_critic_loss, 
                            avg_actor_loss, 
                            avg_entropy_loss,
                            kl_divergence
                        )
                    )

            if self.trained_epochs % 100 == 0:
                self.save_training_status()
                
            self.memory.clear()

    def calculate_losses(self, states, actions, action_log_probs, advantages, value_targets, prev_values, dones):

        states.detach_()
        actions.detach_()
        advantages.detach_()
        action_log_probs.detach_()
        value_targets.detach_()
        prev_values.detach_()
        dones.detach_()
        
        dists, values = self.a2c.forward(states)
        values = values.squeeze(-1)
        curr_action_log_probs = dists.log_prob(actions)

        eps_clip_decayed = self.a2c.decayer(self.trained_epochs)

        g = torch.exp(curr_action_log_probs - action_log_probs)
        clipped_surrogate_loss = -torch.min(
            g * advantages,
            torch.clamp(g, 1 - eps_clip_decayed, 1 + eps_clip_decayed) * advantages
        )

        actor_loss = torch.mean(clipped_surrogate_loss)

        values_clipped = prev_values + \
            torch.clamp(
                values - prev_values,
                -eps_clip_decayed,
                eps_clip_decayed
            )
        value_loss_clipped = (1 - dones) * (values_clipped - value_targets)
        value_loss = (1 - dones) * (values - value_targets)
        critic_loss = torch.max(value_loss, value_loss_clipped).pow(2).mean()

        entropy_loss = dists.entropy().mean()

        p, q = curr_action_log_probs.exp(), action_log_probs.exp()
        kl_divergence_loss = (F.kl_div(p, q).abs() + F.kl_div(q, p).abs()) / 2

        return critic_loss, actor_loss, entropy_loss, kl_divergence_loss

    def collect_mini_batch_from_experience(self, sample_indices, *experience):
        experience = list(experience)

        batch_experience = experience.copy()
        for i, element in enumerate(batch_experience):
            batch_experience[i] = torch.stack([element[j] for j in sample_indices], dim=0)

        return batch_experience

    def get_batch_history(self):

        experience = self.memory.get()

        for idx, element in enumerate(experience):
            temp = torch.stack(element, dim=0).to(self.train_device)
            if idx == 4 or idx == 5:
                experience[idx] = temp.squeeze()
            else:
                experience[idx] = temp.squeeze(-1)
            if idx == 0:
                experience[idx] = temp.squeeze(-1).squeeze(1)

        return experience

    def update_params(self, loss_critic, loss_actor, loss_entropy):
        loss = loss_actor + self.critic_coeff * loss_critic - self.entropy_coeff * loss_entropy
        loss.backward()
        clip_grad_norm_(self.a2c.parameters(), self.max_grad_norm)
        self.a2c.optimizer.step()
        self.a2c.optimizer.zero_grad()
        self.a2c.scheduler.step()

    def save_training_status(self):
        sns.set()
        sns.set_style('darkgrid')
        _fig, axs = plt.subplots(nrows=4, figsize=(10,10))
        plt.subplots_adjust(wspace=1, hspace=1)
        plt.title("Average losses")

        sns.tsplot(data=[l * self.critic_coeff for l in self.avg_critic_loss], value='Loss', condition='Critic', ci='sd', color='g', ax=axs[0])
        sns.tsplot(data=self.avg_actor_loss, value='Loss', condition='Actor', ci='sd', color='b', ax=axs[1])
        sns.tsplot(data=[l * self.entropy_coeff for l in self.avg_entropy_loss], value='Loss', condition='Entropy', ci='sd', color='r', ax=axs[2])
        sns.tsplot(data=self.kl_divergences, value='Loss', condition='KL-divergence', ci='sd', color='y', ax=axs[3])
        plt.savefig(self.file_name + '_losses.png')

    def preprocess(self, observation):

        size = observation.shape[1] // 2
        observation[observation == 43] = 0
        observation[observation == 53] = 0
        observation[observation == 58] = 0

        observation = Image.fromarray(observation)
        observation = functional.resize( observation, size)
        observation = functional.to_grayscale(observation)
        observation = functional.to_tensor(observation)
        observation = functional.normalize(observation, [0], [1])

        observation = observation.squeeze(-1)

        self.prev_obs = torch.cat([self.prev_obs[1:], observation], dim=0)
        stack_ob = self.prev_obs
        stack_ob = stack_ob.float().unsqueeze(0)
        return stack_ob

