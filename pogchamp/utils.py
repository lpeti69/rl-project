import torch
import numpy as np
import matplotlib.pyplot as plt
import random
import seaborn as sns

## TODO: change
class ReplayMemory(object):
    def __init__(self, capacity, *args):
        self.capacity = capacity
        self.size = 0
        self.memory = dict()
        for arg in args:
            self.memory[arg] = []
        self.position = 0

    def push(self, **kwargs):
        for k,v in kwargs.items():
            if self.size < self.capacity:
                self.memory[k].append(None)
            self.memory[k][self.position] = v
        self.position = (self.position + 1) % self.capacity
        self.size = min(self.size + 1, self.capacity)

    def get(self, *args):
        if len(args) == 0:
            args = [k for k,_ in self.memory.items()]
        return [self.memory[k] for k in args]

    def clear(self):
        self.size = 0
        self.position = 0
        for k,_ in self.memory.items():
            self.memory[k] = []
    
    def to(self, device):
        for k,_ in self.memory.items():
            self.memory[k].to(device=device)

## TODO: double check
def plot_rewards(rewards):

    plt.figure(2)
    plt.clf()
    rewards_t = torch.tensor(rewards, dtype=torch.float)

    sns.set()
    sns.lineplot(x='episode', y='reward', data=rewards_t.numpy(), ci='sd')

    # Plot (up to) the first 5 runs, to illustrate the variance
    plt.title('Training performance')
    plt.savefig('training.png')
    plt.xlabel('Episode')
    plt.ylabel('Cumulative reward')
    
    # Take 100 episode averages and plot them too
    if len(rewards_t) >= 100:
        means = rewards_t.unfold(0, 100, 1).mean(1).view(-1)
        means = torch.cat((torch.zeros(99), means))
        plt.plot(means.numpy())

    plt.pause(0.001)  # pause a bit so that plots are updated

def discount_rewards(r, gamma):
    discounted_r = torch.zeros_like(r)
    running_add = 0
    for t in reversed(range(0, r.size(-1))):
        running_add = running_add * gamma + r[t]
        discounted_r[t] = running_add
    return discounted_r

def normalized_rewards(rewards, gamma):
    rewards = discount_rewards(rewards, gamma)
    return normalize(rewards)

def normalize(data):
    eps = np.finfo(np.float32).eps.item()
    data -= torch.mean(data)
    data /= (torch.std(data) + eps)
    return data

def generalized_advantage_estimate(td_error, gamma, _lambda = 0.92):
    return discount_rewards(td_error, gamma * _lambda)


    