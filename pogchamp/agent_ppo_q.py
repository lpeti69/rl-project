import torch
import numpy as np
from torch.distributions import Categorical
from .models_ppo_q import A2CNet
from .utils import ReplayMemory, normalized_rewards
from torch.functional import F
import random

## TODO: save multiple timestamps prior
class Agent(object):
    def __init__(self, train_device = 'cpu', load_params = 'True', is_training = True):
        self.train_device = train_device
        self.gamma = 0.98
        self.a2c = A2CNet(device=train_device).to(self.train_device)
        self.prev_obs = None
        self.eps_clip = 0.2
        self.is_training = is_training
        self.memory = ReplayMemory(capacity=10000)
        if load_params:
            state_dict = torch.load("pogchamp/model_pogchamp_ppo.mdl", map_location=lambda storage, loc: storage)
            self.a2c.load_state_dict(state_dict)
        self.a2c.train(is_training)

    def get_action(self, observation):
        x = self.preprocess(observation).to(self.train_device)
        action_dist, value, q_values = self.a2c.forward(x)

        action = action_dist.sample() if self.is_training else torch.argmax(action_dist.probs)
        action_log_probs = action_dist.log_prob(action)

        entropy = 0.0
        if self.is_training:
            entropy = self.get_entropy(action_dist.probs)

        return x, action, action_log_probs, entropy, q_values[action], value

    def store(self, **kwargs):
        self.memory.push(**kwargs)

    def reset(self):
        self.prev_obs = None

    def get_name(self):
        return 'PogChamp'

    def update(self, num_of_update = 5):
        if self.is_training:
            for _ in range(num_of_update):
                states, actions, action_log_probs, entropys, rewards, values = self.get_batch_history()

                disc_rewards = normalized_rewards(rewards, gamma = self.gamma)
                advantage = disc_rewards - values

                ## TODO: Add Q function
                ## TODO: Add GAE
                loss_critic = self.a2c.critic_loss(disc_rewards, values)
                loss_actor = self.surrogate_clipped_loss(states, actions, action_log_probs, advantage.detach())
                loss_entropy = self.entropy_loss(entropys)
                self.update_params(loss_critic, loss_actor, loss_entropy)
                print('Critic loss: {}, Actor loss: {}, Entropy loss: {}'.format(loss_critic, loss_actor, loss_entropy))
            self.memory.clear()

    def get_batch_history(self):
        memory = self.memory.get()
        states, actions, action_log_probs, entropys, rewards, values = memory['states'], memory['actions'], memory['action_log_probs'], memory['entropys'], memory['rewards'], memory['values']
        indexes = random.sample(range(len(action_log_probs)), min(self.memory.capacity, len(action_log_probs)))

        states = torch.stack([states[i] for i in indexes], dim=0).to(self.train_device).squeeze(-1)
        actions = torch.stack([actions[i] for i in indexes], dim=0).to(self.train_device).squeeze(-1)
        action_log_probs = torch.stack([action_log_probs[i] for i in indexes], dim=0).to(self.train_device).squeeze(-1)
        entropys = torch.stack([entropys[i] for i in indexes], dim=0).to(self.train_device).squeeze(-1)
        rewards = torch.stack([rewards[i] for i in indexes], dim=0).to(self.train_device).squeeze(-1)
        values =  torch.stack([values[i] for i in indexes], dim=0).to(self.train_device).squeeze()
        return states.squeeze(1), actions, action_log_probs, entropys, rewards, values

    def surrogate_clipped_loss(self, states, actions, action_log_probs, advantages):
        dists, _values = self.a2c.forward(states)
        curr_action_log_probs = dists.log_prob(actions)

        g = torch.exp(curr_action_log_probs - action_log_probs)

        loss = -torch.min(
            g * advantages,
            torch.clamp(g, 1 - self.eps_clip, 1 + self.eps_clip) * advantages
        )
        loss = torch.mean(loss)
        return loss

    def get_entropy(self, action_probs):
        p = action_probs
        log_2_3 = torch.log2(torch.FloatTensor([3]))
        return -(p * torch.log2(p + 1e-12) / log_2_3).sum()

    def entropy_loss(self, entropys):
        return entropys.mean()

    def update_params(self, loss_critic, loss_actor, loss_entropy, critic_coeff = 0.4, entropy_coeff = 0.2):
        loss = loss_actor + critic_coeff * loss_critic + entropy_coeff * loss_entropy
        loss.backward(retain_graph=True)
        self.a2c.optimizer.step()
        self.a2c.optimizer.zero_grad()

    def preprocess(self, observation):
        ## TODO: downscale

        observation = observation[::2,::2,:].mean(axis=-1)
        observation = np.expand_dims(observation, axis=-1)
        if self.prev_obs is None:
            self.prev_obs = observation
        stack_ob = np.concatenate((self.prev_obs, observation), axis=-1)
        stack_ob = torch.from_numpy(stack_ob).float().unsqueeze(0)
        stack_ob = stack_ob.transpose(1, 3)
        self.prev_obs = observation
        return stack_ob

