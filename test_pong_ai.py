"""
This is an example on how to use the two player Wimblepong environment with one
agent and the SimpleAI
"""
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--headless", action="store_true", help="Run in headless mode")
parser.add_argument("--housekeeping", action="store_true", help="Plot, player and ball positions and velocities at the end of each episode")
parser.add_argument("--is_testing", action="store_true", help="Set if you want to test the model")
parser.add_argument("--load_params", action="store_true", help="Set if you want to load params")
parser.add_argument("--fps", type=int, help="FPS for rendering", default=90)
parser.add_argument("--scale", type=int, help="Scale of the rendered game", default=2)
parser.add_argument("--device", type=str, help="Training device", default='cuda')
parser.add_argument("--use_ppo", action="store_true", help="Set to use Proximal Policy optimization")
parser.add_argument("--model", type=str, help="Model to use", default='wo_last_linear')
args = parser.parse_args()

if __name__ == "__main__":
    args = parser.parse_args()
    if args.model == 'dqn':
        from pogchamp_deepqn.train_ppo_single import Train
    else:
        if args.use_ppo:
            if args.model == 'pg':
                from pogchamp_ppo_wo_critic.train_ppo_single import Train
            elif args.model == 'op':
                from pogchamp.train_ppo_single_maybe import Train
            else:
                from pogchamp.train_ppo_single import Train
        else:
            from pogchamp.train_single import Train
    trainer = Train(
        args.headless,
        args.housekeeping,
        args.fps,
        args.scale,
        args.device,
        not args.is_testing,
        args.load_params
    )
    trainer.run()