import torch
import numpy as np
from torch.distributions import Categorical
from .models_ppo import A2CNet
from .utils import ReplayMemory, normalize, discount_rewards, generalized_advantage_estimate
from torch.functional import F
import random
from copy import deepcopy
from torch.autograd import Variable
from torch.nn import SmoothL1Loss
from torch.nn.utils import clip_grad_norm_
import seaborn as sns
import matplotlib.pyplot as plt

## TODO: save multiple timestamps prior
class Agent(object):
    def __init__(self, train_device = 'cpu', load_params = 'True', is_training = True):
        self.train_device = train_device
        self.gamma = 0.99
        self.a2c = A2CNet(device=train_device).to(self.train_device)
        self.prev_obs = None
        self.eps_clip = 0.2
        self.iterations = 0
        self.K_epochs = 5
        self.T = self.K_epochs * 32
        self.mini_batch_size = 32
        self.entropy_coeff = 0.01
        self.is_training = is_training
        self.losses = ReplayMemory(250, 'cl', 'al', 'el')
        self.avg_critic_loss = []
        self.avg_actor_loss = []
        self.avg_entropy_loss = []
        self.memory = ReplayMemory(capacity=self.T)
        if load_params:
            state_dict = torch.load(
                "pogchamp/model_pogchamp_ppo_pg.mdl", 
                map_location=lambda storage, loc: storage
            )
            self.a2c.load_state_dict(state_dict)
        self.a2c.train(is_training)

    def get_action(self, observation):
        action_dist = self.a2c.forward(observation)

        action = action_dist.sample() if self.is_training else torch.argmax(action_dist.probs)
        action_log_probs = action_dist.log_prob(action)

        return action, action_log_probs

    def store(self, **kwargs):
        return self.memory.push(**kwargs)

    def reset(self):
        self.prev_obs = None

    def get_name(self):
        return 'PogChamp'

    def batch_update(self, K_epochs = 5):
        if self.is_training:

            states, actions, action_log_probs, advantages = \
                self.get_batch_history()

            #next_values = torch.cat([values[1:], next_value[0]])
            #next_values = (1 - dones) * next_values
            #q_values = rewards + self.gamma * next_values

            #td_errors = q_values - values
            #advantages = generalized_advantage_estimate(td_errors, gamma=self.gamma, _lambda=0.96)\
            #    .to(self.train_device)
            ### TODO: change advantage update
            #rewards = discount_rewards(rewards, gamma = self.gamma)
            #advantages = rewards - values

            #discounted_rewards = discount_rewards(rewards, gamma=self.gamma)
            #rewards = advantages + values

            #advantages = normalize(advantages)

            sample_indeces = random.sample(
                range(min(self.memory.size, self.T)), 
                min(self.memory.size, self.K_epochs * self.mini_batch_size)
            )

            for i in range(self.K_epochs):
                indice_list_size = min(self.memory.size // self.K_epochs, self.mini_batch_size)
                mini_batch_indeces = sample_indeces[
                    indice_list_size * i :
                    indice_list_size * (i + 1)
                ]

                mb_states, mb_actions, mb_action_log_probs, \
                           mb_advantages = \
                    self.collect_mini_batch_from_experience(
                        mini_batch_indeces,
                        states,
                        actions,
                        action_log_probs,
                        advantages
                    )

                loss_actor, loss_entropy = self.calculate_losses(
                    mb_states,
                    mb_actions,
                    mb_action_log_probs,
                    mb_advantages
                )

                self.losses.push(
                    al = loss_actor.data,
                    el = loss_entropy.data
                )

                self.update_params(loss_actor, loss_entropy)

                #print(
                #    'Training Episode: {}, Critic loss: {:.3f}, Actor loss: {:.3f}, Entropy loss: {:.3f}'
                #    .format(
                #        self.iterations,
                #        loss_critic.data, 
                #        loss_actor.data, 
                #        loss_entropy.data
                #    )
                #)
            
            lh = self.losses.get()
            avg_actor_loss, avg_entropy_loss = \
                torch.FloatTensor(lh['al']).mean().data, \
                torch.FloatTensor(lh['el']).mean().data

            self.avg_actor_loss.append(abs(avg_actor_loss))
            self.avg_entropy_loss.append(avg_entropy_loss)
            epoch = self.iterations // self.K_epochs

            if self.iterations % 100 == 0:
                print(
                    'Epoch: {}, Actor loss: {:.3f}, Entropy loss: {:.3f}'
                    .format(
                        epoch,
                        avg_actor_loss, 
                        avg_entropy_loss
                    )
                )

            if epoch % 100 == 0:
                sns.set()
                sns.set_style('darkgrid')
                _fig, axs = plt.subplots(nrows=2, figsize=(10,10))
                plt.subplots_adjust(wspace=1, hspace=1)
                plt.title("Average losses")

                sns.tsplot(data=self.avg_actor_loss, value='Loss', condition='Actor', ci='sd', color='b', ax=axs[0])
                sns.tsplot(data=self.avg_entropy_loss, value='Loss', condition='Entropy', ci='sd', color='r', ax=axs[1])
                plt.savefig('training_ppo_pg.png')

    def calculate_losses(self, states, actions, action_log_probs, advantages):

        states.detach_()
        actions.detach_()
        advantages.detach_()
        action_log_probs.detach_()
        
        dists = self.a2c.forward(states)
        curr_action_log_probs = dists.log_prob(actions)

        g = torch.exp(curr_action_log_probs - action_log_probs)
        clipped_surrogate_loss = -torch.min(
            g * advantages,
            torch.clamp(g, 1 - self.eps_clip, 1 + self.eps_clip) * advantages
        )

        actor_loss = torch.mean(clipped_surrogate_loss)

        entropy_loss = dists.entropy().mean()

        return actor_loss, entropy_loss

    def collect_mini_batch_from_experience(self, sample_indices, *experience):
        experience = list(experience)

        batch_experience = experience.copy()
        for i, element in enumerate(batch_experience):
            batch_experience[i] = torch.stack([element[j] for j in sample_indices], dim=0)

        return batch_experience

    def get_batch_history(self):

        memory = self.memory.get()
        experience = memory['states'], memory['actions'], \
                     memory['action_log_probs'], memory['advantages']
        experience = list(experience)

        for idx, element in enumerate(experience):
            temp = torch.stack(element, dim=0).to(self.train_device)
            if idx == 0:
                experience[idx] = temp.squeeze(-1).squeeze(1)
            else:
                experience[idx] = temp.squeeze(-1)
        return experience

    def update_params(self, loss_actor, loss_entropy):
        self.iterations += 1
        loss = loss_actor - self.entropy_coeff * loss_entropy
        loss.backward()
        #clip_grad_norm_(self.a2c.parameters(), 0.75)
        self.a2c.optimizer.step()
        self.a2c.optimizer.zero_grad()

    def preprocess(self, observation):
        observation = observation[::2,::2,:]
        observation[observation == 43] = 0
        observation[observation == 53] = 0
        observation[observation == 58] = 0
        observation[observation != 0] = 1
        observation = observation[:,:,0]

        observation = np.expand_dims(observation, axis=-1)
        if self.prev_obs is None:
            self.prev_obs = observation
        stack_ob = np.concatenate((self.prev_obs, observation), axis=-1)
        stack_ob = torch.from_numpy(stack_ob).float().unsqueeze(0)
        stack_ob = stack_ob.transpose(1, 3)
        self.prev_obs = observation
        return stack_ob

