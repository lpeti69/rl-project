import torch
import numpy as np
from torch.distributions import Categorical
from .models import A2CNet
from .utils import ReplayMemory, normalized_rewards

## TODO: save multiple timestamps prior
class Agent(object):
    def __init__(self, train_device = 'cpu', load_params = 'True', is_training = True):
        self.train_device = train_device
        self.gamma = 0.98
        self.a2c = A2CNet(device=train_device).to(self.train_device)
        self.prev_obs = None
        self.is_training = is_training
        self.memory = ReplayMemory(capacity=10000)
        if load_params:
            state_dict = torch.load("pogchamp/model_pogchamp.mdl", map_location=lambda storage, loc: storage)
            self.a2c.load_state_dict(state_dict)
        self.a2c.train(is_training)

    def get_action(self, observation):
        x = self.preprocess(observation).to(self.train_device)
        action_dist, value = self.a2c.forward(x)

        action = action_dist.sample() if self.is_training else torch.argmax(action_dist.probs)
        action_log_probs = -action_dist.log_prob(action)
        return action, action_dist.probs, action_log_probs, value

    def store(self, **kwargs):
        self.memory.push(**kwargs)

    def reset(self):
        self.prev_obs = None

    def get_name(self):
        return 'PogChamp'

    ## TODO
    def update(self):
        memory = self.memory.get()
        action_probs, action_log_probs, rewards, values = memory['action_probs'], memory['action_log_probs'], memory['rewards'], memory['values']
        action_probs = torch.stack(action_probs, dim=0).to(self.train_device).squeeze(-1)
        action_log_probs = torch.stack(action_log_probs, dim=0).to(self.train_device).squeeze(-1)
        rewards = torch.stack(rewards, dim=0).to(self.train_device).squeeze(-1)
        values =  torch.stack(values, dim=0).to(self.train_device).squeeze()
        self.memory.clear()

        disc_rewards = normalized_rewards(rewards, gamma = self.gamma)

        advantage = disc_rewards - values
        loss_critic = self.a2c.critic_loss(disc_rewards, values)
        loss_actor = self.a2c.actor_loss(action_log_probs, advantage.detach())
        entropy = self.get_entropy(action_probs)
        if self.is_training:
            self.update_params(loss_critic, loss_actor, entropy)
            print('Critic loss: {}, Actor loss: {}, Entropy: {}'.format(loss_critic, loss_actor, entropy))

    def get_entropy(self, action_probs):
        p = action_probs
        return -(p * torch.log2(p)).sum()


    def update_params(self, loss_critic, loss_actor, entropy, critic_coeff = 0.5, entropy_coeff = 0.25):
        loss = loss_actor + critic_coeff * loss_critic - entropy_coeff * entropy
        loss.backward()
        self.a2c.optimizer.step()
        self.a2c.optimizer.zero_grad()

    def preprocess(self, observation):
        ## TODO: downscale 200 x 200 -> 100 x 100
        observation = observation.mean(axis=-1)
        observation = np.expand_dims(observation, axis=-1)
        if self.prev_obs is None:
            self.prev_obs = observation
        ## append more prev obs np.con.
        stack_ob = np.concatenate((self.prev_obs, observation), axis=-1)
        stack_ob = torch.from_numpy(stack_ob).float().unsqueeze(0)
        stack_ob = stack_ob.transpose(1, 3)
        self.prev_obs = observation
        return stack_ob

