import torch
import numpy as np
from torch.distributions import Categorical
from .models_ppo_wo_last_lin import A2CNet
from .utils import ReplayMemory, normalize, discount_rewards, generalized_advantage_estimate
from torch.functional import F
import random
from copy import deepcopy
from torch.autograd import Variable
from torch.nn import SmoothL1Loss
from torch.nn.utils import clip_grad_norm_
import seaborn as sns
import matplotlib.pyplot as plt

## TODO: save multiple timestamps prior
class Agent(object):
    def __init__(self, train_device = 'cpu', load_params = 'True', is_training = True):
        self.train_device = train_device
        self.gamma = 0.99
        self.a2c = A2CNet(device=train_device).to(self.train_device)
        self.prev_obs = None
        self.eps_clip = 0.2
        self.T = 5 * 512
        self.iterations = 0
        self.K_epochs = 5
        self.mini_batch_size = 512
        self.entropy_coeff = 0.01
        self.critic_coeff = 0.5
        self.is_training = is_training
        self.losses = ReplayMemory(250, 'cl', 'al', 'el')
        self.avg_critic_loss = []
        self.avg_actor_loss = []
        self.avg_entropy_loss = []
        self.memory = ReplayMemory(capacity=self.T)
        if load_params:
            state_dict = torch.load(
                "pogchamp/model_pogchamp_ppo_gae_without_last_linear.mdl", 
                map_location=lambda storage, loc: storage
            )
            self.a2c.load_state_dict(state_dict)
        self.a2c.train(is_training)

    def get_action(self, observation):
        action_dist, value = self.a2c.forward(observation)

        action = action_dist.sample() if self.is_training else torch.argmax(action_dist.probs)
        action_log_probs = action_dist.log_prob(action)

        entropy = 0.0
        if self.is_training:
            entropy = action_dist.entropy()
            ## self.get_entropy(action_dist.probs)

        return action, action_log_probs, entropy, value

    def store(self, **kwargs):
        return self.memory.push(**kwargs)

    def reset(self):
        self.prev_obs = None

    def get_name(self):
        return 'PogChamp'

    def update(self, num_of_update = 4):
        if self.is_training:
            loss_critic, loss_actor, loss_entropy = \
                torch.tensor(0.0).to(self.train_device), \
                torch.tensor(0.0).to(self.train_device), \
                torch.tensor(0.0).to(self.train_device)

            memory = self.memory.get()
            m_states, m_next_states, m_actions, m_action_log_probs, m_entropys, m_rewards, m_values, m_dones = \
                memory['states'], memory['next_states'], memory['actions'], memory['action_log_probs'], memory['entropys'], memory['rewards'], memory['values'], memory['dones']

            next_states = torch.stack(m_next_states, dim=0).to(self.train_device).squeeze()
            _dists, next_values = self.a2c.forward(next_states)
            m_next_values = [torch.abs(1 - m_dones[i]).to(self.train_device) * next_values[i] for i in range(len(m_dones))]

            for _ in range(num_of_update):
                states, actions, action_log_probs, entropys, rewards, values, next_values = \
                    self.get_batch_history(
                        m_states,
                        m_actions,
                        m_action_log_probs,
                        m_entropys,
                        m_rewards,
                        m_values,
                        m_next_values
                    )

                #q_values = rewards + self.gamma * next_values.detach()
                #td_errors = q_values - values
                #advantages = generalized_advantage_estimate(td_errors, gamma=self.gamma, _lambda=0.25)\
                #    .to(self.train_device)
                ##advantages = td_errors.to(self.train_device)

                ## TODO: Add Q function
                loss_critic += self.a2c.critic_loss(q_values, values)
                loss_actor += self.surrogate_clipped_loss(states, actions, action_log_probs, advantages.detach())
                loss_entropy += self.entropy_loss(entropys)
            loss_critic /= num_of_update
            loss_actor /= num_of_update
            loss_entropy /= num_of_update
            self.update_params(loss_critic, loss_actor, loss_entropy)
            print(
                'Critic loss: {:.3f}, Actor loss: {:.3f}, Entropy loss: {:.3f}'
                .format(loss_critic.data, loss_actor.data, loss_entropy.data)
            )
            #self.memory.clear()

    def batch_update(self, next_value, K_epochs = 5):
        if self.is_training:

            states, actions, action_log_probs, rewards, values, dones = \
                self.get_batch_history()

            next_values = torch.cat([values[1:], next_value[0]])
            next_values = (1 - dones) * next_values
            q_values = rewards + self.gamma * next_values

            td_errors = q_values - values
            advantages = generalized_advantage_estimate(td_errors, gamma=self.gamma, _lambda=0.96)\
                .to(self.train_device)

            #discounted_rewards = discount_rewards(rewards, gamma=self.gamma)
            rewards = advantages + values

            advantages = normalize(advantages)

            sample_indeces = random.sample(
                range(self.T), 
                self.K_epochs * self.mini_batch_size
            )
            ## TODO: add sampling

            avg_critic_loss, avg_actor_loss, avg_entropy_loss = [],[],[]
            for i in range(K_epochs):

                mini_batch_indeces = sample_indeces[
                    self.mini_batch_size * i :
                    self.mini_batch_size * (i + 1)
                ]

                mb_states, mb_actions, mb_action_log_probs, \
                           mb_advantages, mb_rewards = \
                    self.collect_mini_batch_from_experience(
                        mini_batch_indeces,
                        states,
                        actions,
                        action_log_probs,
                        advantages,
                        rewards
                    )

                loss_critic, loss_actor, loss_entropy = self.calculate_losses(
                    mb_states,
                    mb_actions,
                    mb_action_log_probs,
                    mb_advantages,
                    mb_rewards
                )

                self.losses.push(
                    cl = loss_critic.data,
                    al = loss_actor.data,
                    el = loss_entropy
                )

                self.update_params(loss_critic, loss_actor, loss_entropy)

                print(
                    'Training Episode: {}, Critic loss: {:.3f}, Actor loss: {:.3f}, Entropy loss: {:.3f}'
                    .format(
                        self.iterations,
                        loss_critic.data, 
                        loss_actor.data, 
                        loss_entropy.data
                    )
                )
            
            lh = self.losses.get()
            avg_critic_loss, avg_actor_loss, avg_entropy_loss = \
                torch.FloatTensor(lh['cl']).mean().data, \
                torch.FloatTensor(lh['al']).mean().data, \
                torch.FloatTensor(lh['el']).mean().data

            self.avg_critic_loss.append(avg_critic_loss)
            self.avg_actor_loss.append(abs(avg_actor_loss))
            self.avg_entropy_loss.append(avg_entropy_loss)
            epoch = self.iterations // self.K_epochs

            print(
                'Running averages\n\tEpoch: {}, Critic loss: {:.3f}, Actor loss: {:.3f}, Entropy loss: {:.3f}'
                .format(
                    epoch,
                    avg_critic_loss, 
                    avg_actor_loss, 
                    avg_entropy_loss
                )
            )

            if epoch % 2 == 0:
                sns.set()
                sns.set_style('darkgrid')
                _fig, axs = plt.subplots(nrows=3, figsize=(10,10))
                plt.subplots_adjust(wspace=1, hspace=1)
                plt.title("Average losses")

                sns.tsplot(data=[l * self.critic_coeff for l in self.avg_critic_loss], value='Loss', condition='Critic', ci='sd', color='g', ax=axs[0])
                sns.tsplot(data=self.avg_actor_loss, value='Loss', condition='Actor', ci='sd', color='b', ax=axs[1])
                sns.tsplot(data=[l * self.entropy_coeff for l in self.avg_entropy_loss], value='Loss', condition='Entropy', ci='sd', color='r', ax=axs[2])
                plt.savefig('training_ppo_gae_losses_wo_last_linear.png')

    def calculate_losses(self, states, actions, action_log_probs, advantages, rewards):

        states.detach_()
        actions.detach_()
        advantages.detach_()
        action_log_probs.detach_()
        rewards.detach_()
        
        dists, values = self.a2c.forward(states)
        values = values.squeeze(-1)
        curr_action_log_probs = dists.log_prob(actions)

        g = torch.exp(curr_action_log_probs - action_log_probs)
        clipped_surrogate_loss = -torch.min(
            g * advantages,
            torch.clamp(g, 1 - self.eps_clip, 1 + self.eps_clip) * advantages
        )

        actor_loss = torch.mean(clipped_surrogate_loss)

        critic_loss = F.smooth_l1_loss(rewards, values)
        entropy_loss = dists.entropy().mean()

        return critic_loss, actor_loss, entropy_loss

    def collect_mini_batch_from_experience(self, sample_indices, *experience):
        experience = list(experience)

        batch_experience = experience.copy()
        for i, element in enumerate(batch_experience):
            batch_experience[i] = torch.stack([element[j] for j in sample_indices], dim=0)

        return batch_experience

    def get_batch_history(self):

        memory = self.memory.get()
        experience = memory['states'], memory['actions'], \
                     memory['action_log_probs'], memory['rewards'], \
                     memory['values'], memory['dones']
        self.memory.clear()
        experience = list(experience)

        for idx, element in enumerate(experience):
            temp = torch.stack(element, dim=0).to(self.train_device)
            if idx == 4:
                experience[idx] = temp.squeeze()
            else:
                experience[idx] = temp.squeeze(-1)
            if idx == 0:
                experience[idx] = temp.squeeze(-1).squeeze(1)

        return experience

    def get_entropy(self, action_probs):
        p = action_probs
        log_2_3 = torch.log2(torch.FloatTensor([3])).to(self.train_device)
        return -(p * torch.log2(p + 1e-12) / log_2_3).sum()

    def entropy_loss(self, entropys):
        return entropys.mean()

    def update_params(self, loss_critic, loss_actor, loss_entropy):
        self.iterations += 1
        loss = loss_actor + self.critic_coeff * loss_critic - self.entropy_coeff * loss_entropy
        loss.backward()
        #clip_grad_norm_(self.a2c.parameters(), 0.75)
        self.a2c.optimizer.step()
        self.a2c.optimizer.zero_grad()

    def preprocess(self, observation):
        observation = observation[::2,::2,].mean(axis=-1)
        observation = np.expand_dims(observation, axis=-1)
        if self.prev_obs is None:
            self.prev_obs = observation
        stack_ob = np.concatenate((self.prev_obs, observation), axis=-1)
        stack_ob = torch.from_numpy(stack_ob).float().unsqueeze(0)
        stack_ob = stack_ob.transpose(1, 3)
        self.prev_obs = observation
        return stack_ob

