import torch
import torch.nn.functional as F
from torch import nn
from torch.distributions import Categorical
from torch.autograd import Variable
import numpy as np
from collections import OrderedDict

class A2CNet(nn.Module):
    def __init__(self, action_space = 3, image_size = 200, device = 'cuda', head_hidden_space = 32, n_conv_layers = 3):
        super(A2CNet, self).__init__()
        self.action_space = action_space
        self.train_device = device
        self.head_hidden_space = head_hidden_space

        self.body_end_size = int(image_size * 2**-n_conv_layers - 1)
        self.feature_space = 2**(n_conv_layers - 1) * 32
        self.body = self.build_body()
        #self.set_body_layers()
        self.actor = self.build_actor_head()
        self.middle_linear = nn.Linear(64*11*11, 256)

        self.init_weights()
        self.optimizer = torch.optim.RMSprop(self.parameters(), lr=9e-4)
    
    def set_body_layers(self):
        self.conv1 = nn.Conv2d(2, 32, 3, 2)
        self.spatial_soft_max = SpatialSoftmax(64, 24, 24, self.train_device)
        self.conv3 = nn.Conv2d(32, 64, 3, 2)
        self.max_pool = nn.MaxPool2d(4, 4)

    def build_body(self):  
        ## TODO: add multiple images   

        return nn.Sequential(
                nn.Conv2d(2, 16, 6, 2),
                nn.ReLU(),
                nn.Conv2d(16, 32, 4, 2),
                nn.ReLU(),
                nn.Conv2d(32, 64, 3, 2),
                nn.ReLU()
                #SpatialSoftmax(32, 23, 23, self.train_device)
            )

    def bbody(self, x):
        x = self.conv1(x)
        x = F.relu(x)

        #print(x.shape)
        x = self.conv3(x)
        x = F.relu(x)
        fk = self.spatial_soft_max(x)



        #fv = self.conv3(x)
        #fv = self.max_pool(fv)
        ## n x 128 x 5 x 5
        ##print(fv.shape)
        #fv = fv.view(fv.shape[0], -1)
        ##print(fv.shape)
        ##print(fk.shape)
        #f = torch.cat([fk, fv], dim=1)

        return fk


    def build_actor_head(self):
        return nn.Sequential(
            #nn.Linear(
            #    2 * self.feature_space,
            #    self.head_hidden_space
            #),
            #nn.ReLU(),
            nn.Linear(
                256,
                self.action_space
            )
        )

    def init_weights(self):
        for m in self.modules():
            if type(m) in [nn.Linear, nn.Conv2d]:
                nn.init.normal_(m.weight)
                nn.init.zeros_(m.bias)

    def forward(self, x):
        x = self.body(x)
        x = x.view(-1, 64*11*11)
#
        x = self.middle_linear(x)
        x_mean = self.actor(x)
        
        x_probs = F.softmax(x_mean, dim=-1)
        dist = Categorical(x_probs)

        return dist


class SpatialSoftmax(nn.Module):
    def __init__(self, channel, height, width, train_device):
        super(SpatialSoftmax, self).__init__()
        self.height = height
        self.width = width
        self.channel = channel
        self.train_device = train_device

        pos_x, pos_y = np.meshgrid(
            np.linspace(-1., 1., self.height),
            np.linspace(-1., 1., self.width)
        )

        pos_x = torch.from_numpy(pos_x.reshape(self.height*self.width)).float()
        pos_y = torch.from_numpy(pos_y.reshape(self.height*self.width)).float()
        self.pos_x = pos_x
        self.pos_y = pos_y

    def forward(self, feature):
        feature = feature.view(-1, self.height*self.width)
        softmax_attention = F.softmax(feature, dim=-1)

        expected_x = torch.sum(Variable(self.pos_x).to(self.train_device)*softmax_attention.to(self.train_device), dim=1, keepdim=True)
        expected_y = torch.sum(Variable(self.pos_y).to(self.train_device)*softmax_attention.to(self.train_device), dim=1, keepdim=True)
        expected_xy = torch.cat([expected_x, expected_y], 1)
        
        feature_keypoints = expected_xy.view(-1, self.channel*2)
        return feature_keypoints

#if __name__ == '__main__':
#  data = torch.randn(1,2,200,200)
#  print(data.shape)
#  layer = A2CNet()
#  print(layer(data))