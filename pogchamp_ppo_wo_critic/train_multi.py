import matplotlib.pyplot as plt
from random import randint
import pickle
import gym
import numpy as np
import torch
import argparse
import wimblepong
from .agent import Agent
from .utils import plot_rewards
import pandas as pd
import seaborn as sns

class Train():
    def __init__(
        self,
        headless = False,
        housekeeping = False,
        fps = 30,
        scale = 1,
        train_device = 'cpu',
        is_training = True
    ):
        # Make the environment
        self.env = gym.make("WimblepongVisualMultiplayer-v0")
        self.env.unwrapped.scale = scale
        self.env.unwrapped.fps = fps
        self.housekeeping = housekeeping
        self.headless = headless

        # Number of episodes/games to play
        self.episodes = 100000

        # Define the player
        self.player_id = 1
        self.opponent_id = 2
        # Set up the player here. We used the SimpleAI that does not take actions for now
        self.player = Agent(train_device=train_device, is_training=is_training)
        self.opponent = Agent(train_device='cpu')
        self.env.set_names(self.player.get_name(), "PogChamp Opponent")

        # Housekeeping
        self.win1 = 0
        self.win2 = 0
        self.states, self.reward_history, self.timestemp_history, self.average_reward_history = \
            [], [], [], []
    
    def run(self):

        for episode_number in range(0, self.episodes):
            done = False
            self.timestemps = 0
            state1, state2 = self.env.reset()
            while not done:
                action1, action_probs1, value1 = self.player.get_action(state1)
                action2, action_probs2, value2 = self.opponent.get_action(state2)
                prev_state1, prev_state2 = state1, state2
                (state1, state2), (reward1, reward2), done, _info = \
                    self.env.step((action1, action2))
                self.player.store(
                    states = torch.from_numpy(np.flip(prev_state1,axis=0).copy()),
                    actions = action1,
                    action_probs = action_probs1,
                    next_states = torch.from_numpy(np.flip(state1,axis=0).copy()), 
                    rewards = torch.Tensor([reward1]),
                    dones = torch.Tensor([done]),
                    values = value1
                )
                self.opponent.store(
                    states = torch.from_numpy(np.flip(prev_state2,axis=0).copy()),
                    actions = action2,
                    action_probs = action_probs2,
                    next_states = torch.from_numpy(np.flip(state2,axis=0).copy()), 
                    rewards = torch.Tensor([reward2]),
                    dones = torch.Tensor([done]),
                    values = value2
                )
                self.timestemps += 1
                if self.housekeeping:
                    #self.states.append(state1)
                    pass
                # Count the wins
                if reward1 == 10:
                    self.win1 += 1
                if not self.headless or episode_number > self.episodes + 25:
                    self.env.render()
                if done:
                    self.player.update()
                    plt.close()  # Hides game window
                    if self.housekeeping:
                        #plt.plot(states)
                        #plt.legend(["Player", "Opponent", "Ball X", "Ball Y", "Ball vx", "Ball vy"])
                        #plt.show()
                        #states.clear()
                        self.reward_history.append(10 * reward1 / self.timestemps)
                        self.timestemp_history.append(self.timestemps)
                        if episode_number > 100:
                            avg = np.mean(self.reward_history[-100:])
                        else:
                            avg = np.mean(self.reward_history)
                        self.average_reward_history.append(avg)
                    print("episode {} over. Reward: {:.3f}, Broken WR: {:.3f}"
                            .format(episode_number, reward1 / self.timestemps, self.win1/(episode_number+1)))
                    if episode_number % 5 == 4:
                        self.env.switch_sides()

            if self.housekeeping:
                if (episode_number % 1000 == 0 and episode_number >= 1000) or episode_number == self.episodes - 1:
                    self.show_result(episode_number)

    def save(self):
        _data = pd.DataFrame({"episode": np.arange(len(self.reward_history)),
                            "algorithm": ["PG"]*len(self.reward_history),
                            "reward": self.reward_history})
        torch.save(self.player.a2c.state_dict(), "model_%s.mdl" % 'pogchamp')

    def show_result(self, episode_number):
        sns.set()
        sns.set_style('darkgrid')
        plt.legend(["Reward", "100-episode average"])
        plt.title("Reward history")

        sns.tsplot(data=self.reward_history, value='reward', condition='reward', ci='sd', color='g')
        sns.tsplot(data=self.average_reward_history, value='reward', condition='Avg reward', ci='sd', color='b')
        plt.savefig('training_multi.png')
        
        if episode_number == self.episodes - 1:
            plt.show()
            print("Training finished.")
        if episode_number % 10000 == 0 or episode_number == self.episodes - 1:
            self.save()