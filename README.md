# RL-Project

## TODO:
1.  Preprocess the image (making it grayscale / calculating difference for the last x steps as channels) - **TODO**
    1. Prerocess image -  **Done**
    2. Use one image as history - **Done**
    3. Downscale - **Done**
    3. Use multiple image as history - **TODO**
2.  Create Basic (Convolutional) Neural network as the body layers - **Done**
    1.  Potentially adding spatial softmax in the end - **Done**
    2.  Decrease the complexity - **Done**
3.  Create policy head with softmax distr over action space (layout is arbitrary) - **Done**
4.  Create a critic - **Done**
    1. Use Q value function over the action space - **No improvement**
    2. Use V value function - **Done** 
5.  Create a loss for the crtitic (value head) - **Done**
    1. We can use either the squared error we used for ex5 - **Done**
6. Create an estimation of the advantage function - **Done**
    1. Either disc_rewards - V - **Done**
    2. TD batch - **Done**
    3. Generalized advatange estimation function - **Done**
    5. Proximal policy optimization cost function - **Done**
7. Create the actor loss (adv * act_prob) - **Done**
8. Update the NN based on batch - **Done**
    1. We can use static batch update (like the last 20 steps - for this we need to calculate an estimation of a trajectory duration) - **Done**
    2. We can use dynamic batch size (we include only those steps which has been made after the last touch of the ball made by us) - **Done**
9. We can store the crtic's and the actor's moves (state, action) in an experience buffer which is needed for (5.2 and 6.4) - **Done**

## Post Assignment ##
10. Implement Q-learning adding:
    1. Fixed Q target
        1. Update target network every t epochs
    2. Dueling DQN
        1. Extract Q values to 
            1. A
            2. V
        2. Aggregate them
    3. Prioritized Experience Replay
        1. Create a prob. distribution for the samples
        2. Sample accordingly
        3. Add sample weights when calculating Q_target errors

11. Implement ACER adding:
    1. Q-network for critic instead of V
    2. Implement Retrace estimate of real Q
    3. Add trust region optimization

## TO Understand:
1. Spatial softmax - **Done**
2. Retrace estimator - **Done**
3. Trust region policy method - **Done**
4. Proximal Policy optimization - **Done**
5. DDQN, Dueling DQN, Fixed target - **Done**
6. Prioritized Experience replay - **Done**
7. On-policy vs off-policy - **Done**
8. ACKTR, A3C, A2C, MAC, ACER - **Done**