import torch
import numpy as np
from torch.distributions import Categorical
from .models_ppo import A2CNet
from .utils import ReplayMemory, normalize, discount_rewards, generalized_advantage_estimate
from torch.functional import F
import random
from copy import deepcopy
from torch.autograd import Variable
from torch.nn import SmoothL1Loss
from torch.nn.utils import clip_grad_norm_
import seaborn as sns
import matplotlib.pyplot as plt

## TODO: save multiple timestamps prior
class Agent(object):
    def __init__(self, train_device = 'cpu', load_params = 'True', is_training = True):
        self.train_device = train_device
        self.gamma = 0.99
        self.a2c = A2CNet(device=train_device).to(self.train_device)
        self.prev_obs = None
        self.eps_clip = 0.2
        self.T = 5 * 2048
        self.iterations = 0
        self.K_epochs = 5
        self.mini_batch_size = 64
        self.glie_a = 200
        self.is_training = is_training
        self.losses = ReplayMemory(250, 'cl')
        self.avg_loss = []
        self.memory = ReplayMemory(capacity=self.T)
        if load_params:
            state_dict = torch.load(
                "pogchamp/model_pogchamp_dqn.mdl", 
                map_location=lambda storage, loc: storage
            )
            self.a2c.load_state_dict(state_dict)
        self.a2c.train(is_training)

    def get_action(self, observation, eps):
        if random.random() > eps:
            with torch.no_grad():
                q_values = self.a2c.forward(observation)
                return torch.argmax(q_values)
        else:
            return random.randrange(self.a2c.action_space)

    def store(self, **kwargs):
        return self.memory.push(**kwargs)

    def reset(self):
        self.prev_obs = None

    def get_name(self):
        return 'PogChamp'

    def batch_update(self):
        if self.is_training:

            states, actions, next_states, dones, rewards = \
                self.get_batch_history()

            sample_indeces = random.sample(
                range(min(self.memory.size, self.T)), 
                min(self.memory.size, self.K_epochs * self.mini_batch_size)
            )

            for i in range(self.K_epochs):
                indice_list_size = min(self.memory.size // self.K_epochs, self.mini_batch_size)
                mini_batch_indeces = sample_indeces[
                    indice_list_size * i :
                    indice_list_size * (i + 1)
                ]

                mb_states, mb_actions, mb_next_states, \
                           mb_dones, mb_rewards = \
                    self.collect_mini_batch_from_experience(
                        mini_batch_indeces,
                        states,
                        actions,
                        next_states,
                        dones,
                        rewards
                    )

                loss = self.calculate_losses(
                    mb_states,
                    mb_next_states,
                    mb_actions,
                    mb_rewards,
                    mb_dones,
                )

                self.losses.push(
                    cl = loss.data
                )

                self.update_params(loss)

                #print(
                #    'Training Episode: {}, Critic loss: {:.3f}, Actor loss: {:.3f}, Entropy loss: {:.3f}'
                #    .format(
                #        self.iterations,
                #        loss_critic.data, 
                #        loss_actor.data, 
                #        loss_entropy.data
                #    )
                #)
            
            lh = self.losses.get()
            avg_loss = \
                torch.FloatTensor(lh['cl']).mean().data

            self.avg_loss.append(avg_loss)
            epoch = self.iterations // self.K_epochs

            if self.iterations % 100 == 0:
                print(
                    'Epoch: {}, Loss: {:.3f}'
                    .format(
                        epoch,
                        avg_loss
                    )
                )

            if epoch % 100 == 0:
                sns.set()
                sns.set_style('darkgrid')
                _fig, axs = plt.subplots(nrows=2, figsize=(10,10))
                plt.subplots_adjust(wspace=1, hspace=1)
                plt.title("Losses")

                sns.tsplot(data=self.avg_loss, value='Loss', condition='Critic', ci='sd', color='g', ax=axs[0])
                sns.tsplot(data=torch.FloatTensor(lh['cl']), value='Loss', condition='Critic', ci='sd', color='b', ax=axs[1])
                plt.savefig('training_dqn.png')

    def calculate_losses(self, states, next_states, actions, rewards, dones):

        states.detach_()
        next_states.detach_()
        actions.detach_()
        rewards.detach_()
        dones.detach_()
        next_q_values = self.a2c.forward(next_states)
        q_target = torch.max(next_q_values, dim=1)[0]

        q_target = rewards + self.gamma * (1 - dones) * q_target.detach() 

        q_values = self.a2c.forward(states)
        current_q_values = q_values.gather(1, actions.unsqueeze(1)).squeeze()

        loss = F.smooth_l1_loss(q_target, current_q_values)
        return loss

    def collect_mini_batch_from_experience(self, sample_indices, *experience):
        experience = list(experience)

        batch_experience = experience.copy()
        for i, element in enumerate(batch_experience):
            batch_experience[i] = torch.stack([element[j] for j in sample_indices], dim=0)

        return batch_experience

    def get_batch_history(self):

        memory = self.memory.get()
        experience = memory['states'], memory['actions'], \
                     memory['next_states'], memory['dones'], \
                     memory['rewards']
        experience = list(experience)

        for idx, element in enumerate(experience):
            temp = torch.stack(element, dim=0).to(self.train_device)
            experience[idx] = temp.squeeze(-1)
            if idx == 0 or idx == 2:
                experience[idx] = temp.squeeze(-1).squeeze(1)

        return experience

    def update_params(self, loss):
        self.iterations += 1
        loss.backward()
        clip_grad_norm_(self.a2c.parameters(), 0.75)
        self.a2c.optimizer.step()
        self.a2c.optimizer.zero_grad()

    def preprocess(self, observation):
        observation = observation[::2,::2,:]
        observation[observation == 43] = 0
        observation[observation == 53] = 0
        observation[observation == 58] = 0
        observation[observation != 0] = 1
        observation = observation[:,:,0]

        observation = np.expand_dims(observation, axis=-1)
        if self.prev_obs is None:
            self.prev_obs = observation
        stack_ob = np.concatenate((self.prev_obs, observation), axis=-1)
        stack_ob = torch.from_numpy(stack_ob).float().unsqueeze(0)
        stack_ob = stack_ob.transpose(1, 3)
        self.prev_obs = observation
        return stack_ob

