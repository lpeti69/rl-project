import torch
import numpy as np

def discount_rewards(r, gamma):
    discounted_r = torch.zeros_like(r)
    running_add = 0
    for t in reversed(range(0, r.size(-1))):
        running_add = running_add * gamma + r[t]
        discounted_r[t] = running_add
        print(running_add)
    return discounted_r

def normalized_rewards(rewards, gamma):
    rewards = discount_rewards(rewards, gamma)
    eps = np.finfo(np.float32).eps.item()
    rewards -= torch.mean(rewards)
    rewards /= (torch.std(rewards) + eps)
    return rewards

def generalized_advantage_estimate(td_error, gamma, _lambda = 0.5):
    return normalized_rewards(td_error, gamma * _lambda)

def gae(td_error, gamma, _lambda):
    return normalized_rewards(td_error, gamma * _lambda)

rewards = torch.FloatTensor([1,3,5,6])
values = torch.FloatTensor([1, 3.1, 4.8, 5.7])
q = torch.FloatTensor([1, 2.5, 6, 4.7])

disc_rewards = discount_rewards(rewards, gamma = 0.99)
td_error = q - values

adv1 = gae(td_error, gamma = 0.99, _lambda = 0.92)
adv2 = td_error
adv3 = disc_rewards - values
print(abs(adv1 - adv2))
print(abs(adv1 - adv3))